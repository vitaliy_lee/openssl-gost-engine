use std::fs;
use std::mem;
use std::path::Path;
use std::sync::{Arc, Mutex, Once};

use openssl::cms::{CMSOptions, CmsContentInfo};
use openssl::hash::{hash, MessageDigest};
use openssl::nid::Nid;
use openssl::pkcs12::Pkcs12;
use openssl::pkey::{PKey, Private};
use openssl::x509::crl::X509CRL;
use openssl::x509::store::X509StoreBuilder;
use openssl::x509::verify::X509VerifyFlags;
use openssl::x509::{X509StoreContext, X509};

use openssl_gost_engine::*;

use maplit::hashmap;

#[derive(Clone)]
struct SingletonReader {
    // Since we will be used in many threads, we need to protect
    // concurrent access
    inner: Arc<Mutex<Engine>>,
}

fn singleton() -> SingletonReader {
    // Initialize it to a null value
    static mut SINGLETON: *const SingletonReader = 0 as *const SingletonReader;
    static ONCE: Once = Once::new();

    unsafe {
        ONCE.call_once(|| {
            // Make it
            let engine = init_engine().expect("Инициализировать движок");

            let reader = SingletonReader {
                inner: Arc::new(Mutex::new(engine)),
            };

            // Put it in the heap so it can outlive this call
            let reader_ref = Box::new(reader);

            SINGLETON = mem::transmute(reader_ref);
        });

        // Now we give out a copy of the data that is safe to use concurrently.
        (*SINGLETON).clone()
    }
}

fn init_engine() -> Result<Engine> {
    let engine = Engine::new()?;
    // engine.init()?;

    return Ok(engine);
}

#[test]
fn test_read_private_key() {
    let reader = singleton();
    let _engine = reader.inner.lock().unwrap();

    let result = open_private_key("testdata/tls/keystore/server.key.pem");

    assert!(result.is_ok());
}

#[test]
fn test_generate_private_key() {
    let reader = singleton();
    let engine = reader.inner.lock().unwrap();

    let algorithm = KeyPairAlgorithm::GOST3410_2012_256;
    let param_set = ParamSet::GOST3410_2001_TEST;

    let result = engine.generate_private_key(algorithm, param_set);
    assert!(result.is_ok());

    // let private_key = result.unwrap();
    // println!("{:?}", private_key);

    // let priv_key_data = private_key.private_key_to_pem_pkcs8().unwrap();
    // let pub_key_data = private_key.public_key_to_pem().unwrap();

    // println!("{:?}", String::from_utf8(priv_key_data).unwrap());
    // println!("{:?}", String::from_utf8(pub_key_data).unwrap());
}

#[test]
fn test_sign_verify() {
    let reader = singleton();
    let engine = reader.inner.lock().unwrap();

    let message_digest = MessageDigest::from_nid(Nid::ID_GOSTR3411_2012_256)
        .expect("Создать компонент подсчёта контрольной суммы");

    let algorithm = KeyPairAlgorithm::GOST3410_2012_256;
    let param_set = ParamSet::GOST3410_2001_TEST;

    let private_key = engine
        .generate_private_key(algorithm, param_set)
        .expect("Сгенерировать ключ");

    let mut signer = openssl::sign::Signer::new(message_digest, &private_key)
        .expect("Создать компонент подписи");

    let message = hex::decode("06ccdd07b6d96d0b1c8db44f8865af8daf868c7b").unwrap();

    let signature = signer
        .sign_oneshot_to_vec(&message)
        .expect("Создать подпись");

    let mut verifier = openssl::sign::Verifier::new(message_digest, &private_key)
        .expect("Создать компонент проверки подписи");
    let valid = verifier
        .verify_oneshot(&signature, &message)
        .expect("Проверить подпись");

    assert!(valid);
}

#[test]
fn test_hash() {
    let message = "github.com/hyperledger/fabric";

    let test_data = hashmap![
        Nid::ID_GOSTR3411_2012_256 => "9557b55c9cf5fe3f3dab8c2dffd1510eb4bd3943782b77646874ce6fd362bbef",
        Nid::ID_GOSTR3411_2012_512 => "f0d9a2ba871996f0cf6a189a551dddeff876d0290198632a5cd24800705df1bbb85c3c7107489d191c67c81bac995507d25d214b12692596117998176f2c88ad",
    ];

    let reader = singleton();
    let _engine = reader.inner.lock().unwrap();

    for (nid, expected_digest) in test_data {
        let message_digest =
            MessageDigest::from_nid(nid).expect("Создать компонент подсчёта контрольной суммы");

        let expected = hex::decode(expected_digest).unwrap();

        let digest = hash(message_digest, message.as_bytes()).expect("Создать хэш");
        assert_eq!(&expected, digest.as_ref());
    }
}

#[test]
fn test_verify_cert() {
    let reader = singleton();
    let _engine = reader.inner.lock().unwrap();

    let ca_certificate = open_certificate("testdata/tls/cacerts/ca.cert.pem")
        .expect("Открыть сертификат удостоверяющего центра");

    let certificate = open_certificate("testdata/tls/certs/server.cert.pem")
        .expect("Открыть проверяемый сертификат");

    let empty_crl = open_crl("testdata/tls/crls/empty.crl.pem")
        .expect("Открыть пустой список отозванных сертификатов");

    let flags = X509VerifyFlags::CRL_CHECK | X509VerifyFlags::CRL_CHECK_ALL;

    let mut store_builder = X509StoreBuilder::new().unwrap();
    store_builder.add_cert(&ca_certificate).unwrap();
    store_builder.add_crl(&empty_crl).unwrap();
    store_builder.set_flags(flags).unwrap();
    let store = store_builder.build();

    let mut context = X509StoreContext::new().unwrap();
    context.init(&store, &certificate, None).unwrap();
    let mut valid = context.verify_cert().unwrap();
    assert!(valid);

    let revoked_crl = open_crl("testdata/tls/crls/revoked.crl.pem")
        .expect("Открыть непустой список отозванных сертификатов");

    let mut store_builder = X509StoreBuilder::new().unwrap();
    store_builder.add_cert(&ca_certificate).unwrap();
    store_builder.add_crl(&revoked_crl).unwrap();
    store_builder.set_flags(flags).unwrap();
    let store = store_builder.build();

    context = X509StoreContext::new().unwrap();
    context.init(&store, &certificate, None).unwrap();
    valid = context.verify_cert().unwrap();

    assert!(!valid);
}

#[test]
fn cms_sign_verify() {
    let reader = singleton();
    let _engine = reader.inner.lock().unwrap();

    let ca_certificate = open_certificate(Path::new("testdata/tls/cacerts/ca.cert.pem"))
        .expect("Открыть сертификат");

    let pkcs12 = open_pkcs12(Path::new("testdata/tls/keystore/keystore.p12"))
        .expect("Прочитать PKCS#12 контейнер");

    let key_store = pkcs12.parse("coffee").expect("Открыть PKCS#12 контейнер");

    // Подписать
    let input = String::from("Good day starts with coffee.");

    let sign_flags = CMSOptions::NOSMIMECAP;

    let content_info = CmsContentInfo::sign(
        Some(&key_store.cert),
        Some(&key_store.pkey),
        None,
        Some(input.as_bytes()),
        sign_flags,
    )
    .expect("Создать CMS");

    // let data = content_info.to_pem().unwrap();
    // fs::write("testdata/message.cms.pem", &data).unwrap();

    // let content = content_info.get_content().expect("Get content");
    // println!("{:?}", String::from_utf8(content.to_vec()).unwrap());

    // Проверить
    let verify_flags = CMSOptions::BINARY;

    let mut store_builder = X509StoreBuilder::new().unwrap();
    store_builder.add_cert(&ca_certificate).unwrap();
    let trust_store = store_builder.build();

    let valid = content_info
        .verify(None, &trust_store, verify_flags)
        .expect("Verify CMS");
    assert!(valid);
}

fn open_private_key<P: AsRef<Path>>(
    path: P,
) -> std::result::Result<PKey<Private>, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    PKey::private_key_from_pem(&data).map_err(|err| err.into())
}

fn open_certificate<P: AsRef<Path>>(
    path: P,
) -> std::result::Result<X509, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    X509::from_pem(&data).map_err(|err| err.into())
}

fn open_pkcs12<P: AsRef<Path>>(path: P) -> std::result::Result<Pkcs12, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    Pkcs12::from_der(&data).map_err(|err| err.into())
}

fn open_crl<P: AsRef<Path>>(path: P) -> std::result::Result<X509CRL, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    X509CRL::from_pem(&data).map_err(|err| err.into())
}
