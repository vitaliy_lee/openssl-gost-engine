use std::fmt;

use openssl::error::ErrorStack;
use openssl::pkey::{Id, PKey, PKeyCtx, Private};

const ENGINE_ID: &str = "gost";

pub type Result<T> = std::result::Result<T, ErrorStack>;

pub enum ParamSet {
    // КриптоПРО
    GOST3410_2001_TEST,            // "0"
    GOST3410_2001_CRYPTO_PRO_A,    // "A"
    GOST3410_2001_CRYPTO_PRO_B,    // "B"
    GOST3410_2001_CRYPTO_PRO_C,    // "C"
    GOST3410_2001_CRYPTO_PRO_XCHA, // "XA"
    GOST3410_2001_CRYPTO_PRO_XCHB, // "XB"
    // ТК26
    GOST3410_2012_256_TC26_A, // "TCA"
    GOST3410_2012_256_TC26_B, // "TCB"
    GOST3410_2012_256_TC26_C, // "TCC"
    GOST3410_2012_256_TC26_D, // "TCD"
    GOST3410_2012_512_TC26_A, // "A"
    GOST3410_2012_512_TC26_B, // "B"
    GOST3410_2012_512_TC26_C, // "C"
}

impl fmt::Display for ParamSet {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = match self {
            Self::GOST3410_2001_TEST => "0",
            Self::GOST3410_2001_CRYPTO_PRO_A => "A",
            Self::GOST3410_2001_CRYPTO_PRO_B => "B",
            Self::GOST3410_2001_CRYPTO_PRO_C => "C",
            Self::GOST3410_2001_CRYPTO_PRO_XCHA => "XA",
            Self::GOST3410_2001_CRYPTO_PRO_XCHB => "XB",
            Self::GOST3410_2012_256_TC26_A => "TCA",
            Self::GOST3410_2012_256_TC26_B => "TCB",
            Self::GOST3410_2012_256_TC26_C => "TCC",
            Self::GOST3410_2012_256_TC26_D => "TCD",
            Self::GOST3410_2012_512_TC26_A => "A",
            Self::GOST3410_2012_512_TC26_B => "B",
            Self::GOST3410_2012_512_TC26_C => "C",
        };

        write!(formatter, "{}", text)
    }
}

pub enum KeyPairAlgorithm {
    GOST3410_2012_256,
    GOST3410_2012_512,
}

impl KeyPairAlgorithm {
    fn to_id(&self) -> Id {
        match self {
            Self::GOST3410_2012_256 => Id::GOST3410_2012_256,
            Self::GOST3410_2012_512 => Id::GOST3410_2012_512,
        }
    }
}

pub struct Engine {
    engine: openssl::engine::Engine,
}

impl Engine {
    pub fn new() -> Result<Engine> {
        openssl::init();

        let engine = openssl::engine::Engine::by_id(ENGINE_ID)?;
        engine.init()?;
        engine.set_default(openssl::engine::EngineMethod::ALL)?;

        let gost_engine = Engine { engine };

        return Ok(gost_engine);
    }

    pub fn generate_private_key(
        &self,
        algorithm: KeyPairAlgorithm,
        param_set: ParamSet,
    ) -> Result<PKey<Private>> {
        let ctx = PKeyCtx::new(algorithm.to_id())?;
        ctx.keygen_init()?;
        ctx.ctrl_str("paramset", &param_set.to_string())?;
        ctx.keygen()
    }
}

impl Drop for Engine {
    fn drop(&mut self) {
        self.engine.finish().unwrap();
        self.engine.free().unwrap();
    }
}
